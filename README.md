# pipeline-microservice-lamda

A production-grade pipeline for testing and deploying Serverless microservices including canary deployment to production with rollbacking automatically in case of failures.

See https://medium.com/p/888668bcfe04